package ru.myandin.threads;

import ru.myandin.generics.Order;

import java.util.ArrayList;

// класс хранит в себе ссылку на коллекцию заказов
// Thread - класс потока, Runnable - интерфейс с методом run
public abstract class ACheck extends Thread {
    // ссылка на коллекцию заказов
    protected ArrayList<Order> orders;

    public static final Object mutex = false;
    protected static boolean ongoing = true;

    // потоковая функция
    @Override
    abstract public void run();

    public static void SetPaused(boolean paused) {
        synchronized (mutex) {
            ongoing = !paused;
        }
    }
}
