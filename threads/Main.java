package ru.myandin.threads;


import ru.myandin.basics.*;
import ru.myandin.generics.Order;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Drink> a = new ArrayList<>();

        Tea b;
        b = new Tea();
        b.setName("Lipton");
        b.setCountry_manufacturer("Great Britain");
        b.setDistributor("Lipton Inc.");
        b.setPackage_type(1);
        b.setCost(100);
        a.add(b);

        b = new Tea();
        b.setName("Лисма");
        b.setCountry_manufacturer("Thailand");
        b.setDistributor("Lisma Inc.");
        b.setPackage_type(0);
        b.setCost(45);
        a.add(b);

        b = new Tea();
        b.setName("Brooke Bond");
        b.setCountry_manufacturer("Sri-Lanka");
        b.setDistributor("Brooke Bond Inc.");
        b.setPackage_type(0);
        b.setCost(70);
        a.add(b);

        Coffee f;
        f = new Coffee();
        f.setName("Nescafe");
        f.setCountry_manufacturer("Russia");
        f.setDistributor("Nestle");
        f.setCoffee_type(CoffeeType.arabica);
        f.setCost(90);
        a.add(f);

        f = new Coffee();
        f.setName("Jardin");
        f.setCountry_manufacturer("USA");
        f.setDistributor("Jardin Inc.");
        f.setCoffee_type(CoffeeType.robusta);
        f.setCost(200);
        a.add(f);

        for (int i = 0; i < a.size(); i++) {
            Drink n = a.get(i);
            n.read();
            System.out.println();
        }

        ArrayList<Order> orders = new ArrayList<>();

        Check_InProgress c1 = new Check_InProgress();
        c1.AssignCollection(orders);
        c1.start();

        Check_Done c2 = new Check_Done();
        c2.AssignCollection(orders);
        c2.start();

        Generator g1 = new Generator();
        g1.AssignCollection(orders);
        g1.SetAvailableDrinksCollection(a);
        g1.SetInterval(3500);
        g1.start();

        Generator g2 = new Generator();
        g2.AssignCollection(orders);
        g2.SetAvailableDrinksCollection(a);
        g2.SetInterval(4200);
        g2.start();

        Scanner sc = new Scanner(System.in);
        while (true) {
            System.out.println("Консоль работает в отдельном потоке. Можете в этом убедиться. Введите текст: ");
            String line = sc.nextLine();
            System.out.println("Вы ввели: \"" + line + "\".");
        }
    }
}
