package ru.myandin.threads;

import ru.myandin.generics.Order;
import ru.myandin.generics.StatusOrder;

import java.util.ArrayList;

public class Check_InProgress extends ACheck {
    //потоковая функция
    @Override
    public void run() {
        while (true) {
            boolean generate_next = true;
            synchronized (mutex) {
                if (!ongoing) generate_next = false;
            }

            if (generate_next) {
                int k = 0;
                synchronized (mutex) {
                    // просматриваем список заказов
                    for (int i = 0; i < orders.size(); i++) {
                        // если заказ ждёт
                        if (orders.get(i).getStatus() == StatusOrder.in_progress) {
                            // пометить что он готов
                            orders.get(i).setStatus(StatusOrder.done);
                            k++;
                        }
                    }
                }

                // отладочный вывод (для демонстрации)
                synchronized (mutex) {
                    System.out.println("[Check_InProgress] Served " + k + " waiting orders.");
                    System.out.print("W: ");
                    for (int i = 0; i < orders.size(); i++) {
                        if (orders.get(i).getStatus() == StatusOrder.in_progress)
                            System.out.print(orders.get(i).getId() + " ");
                    }
                    System.out.println();
                    System.out.print("D: ");
                    for (int i = 0; i < orders.size(); i++) {
                        if (orders.get(i).getStatus() == StatusOrder.done)
                            System.out.print(orders.get(i).getId() + " ");
                    }
                    System.out.println();
                    System.out.println();
                }
            }
            try {
                sleep(10000);//10 секунд
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // потоковая функция
    @Override
    public void start() {
        if (orders == null) // кинуть исключение типа IllegalArgumentException
            throw new IllegalArgumentException("Ссылка на список заказов (orders) пустая, невозможно начать");
        super.start();
    }

    // сохраняет ссылку на список заказов внутри потока (т.е. объекта класса Check_InProgress)
    public void AssignCollection(ArrayList<Order> orders) {
        if (this.isAlive()) return; // isAlive проверяет запущен ли поток сейчас
        this.orders = orders;
    }
}
