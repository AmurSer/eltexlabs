package ru.myandin.generics;

import ru.myandin.basics.Drink;

import java.io.Serializable;
import java.time.Instant;
import java.util.*;

public class ShoppingCart<T extends Drink> implements Serializable {//T = Drink
    protected ArrayList<T> items_in_cart;
    protected transient HashSet<T> items_to_find;
    protected transient HashMap<Date, T> items_by_date;

    public ShoppingCart() {
        items_in_cart = new ArrayList<>();
        items_to_find = new HashSet<>();
        items_by_date = new HashMap<>();
    }

    public void add(T a) {
        items_in_cart.add(a);
        items_to_find.add(a);
        items_by_date.put(Date.from(Instant.now()), a);
    }

    public void print() {
        for (int i = 0; i < items_in_cart.size(); i++) {
            items_in_cart.get(i).read();
        }
    }

    public void delete(T a) {
        for (int i = 0; i < items_in_cart.size(); i++) {
            if (items_in_cart.get(i).equals(a)) {
                items_in_cart.remove(i);
            }
        }
    }

    public void delete(UUID b) {
        for (int i = 0; i < items_in_cart.size(); i++) {
            if (items_in_cart.get(i).getID_Drink().equals(b)) {
                items_in_cart.remove(i);
            }
        }
    }

    public T find(UUID b) {
        Iterator<T> iterator = items_to_find.iterator();
        while (iterator.hasNext()) {
            T n = iterator.next();
            if (n.getID_Drink().equals(b))
                return n;
        }
        return null;
    }

}


