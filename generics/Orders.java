package ru.myandin.generics;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedList;

public class Orders<T extends Order> {
    protected LinkedList<T> order_list;

    public Orders() {
        order_list = new LinkedList<>();
    }

    public void Make_purchase(ShoppingCart c, Credentials d) {
        order_list.add((T) new Order(StatusOrder.in_progress, Date.from(Instant.now()),
                Date.from(Instant.now().plus(2, ChronoUnit.DAYS)), c, d));
    }

    public void check_orders() {
        for (int i = 0; i < order_list.size(); i++) {
            if (order_list.get(i).getStatus().equals(StatusOrder.done) ||
                    Date.from(Instant.now()).after(order_list.get(i).getReserved_to())) {
                order_list.remove(i);
            }
        }
    }

    public void print_orders() {
        for (int i = 0; i < order_list.size(); i++) {
            order_list.get(i).print();
        }
    }

    // написать функцию удаления заказа по id
    public void delete_orders(int id) {
        for (int i = 0; i < order_list.size(); i++) {
            if (order_list.get(i).getId() == id) {
                order_list.remove(i);
                return;
            }
        }
    }
}
