package ru.myandin.generics;


import java.util.ArrayList;
import java.util.Scanner;

import ru.myandin.basics.*;

public class Main {


    // args - массив аргументов командной строки
    // String[] args - массив со строками под названием args
    public static void main1(String[] args) {
        // обьявили имя a для Drink
        Drink a;
        //equals сравниваем обьект Coffee с аргументом
        if (args[0].equals("Coffee")) {
            //создаём новый пустой обьект Coffee для Drink c именем a
            a = new Coffee();
        } else if (args[0].equals("Tea")) {
            a = new Tea();
        } else {
            System.out.println("нужно выбрать только чай или кофе");
            return;
        }

        int count = 1; // количество вводимых объектов. по умолчанию 1.
        //length - возвращаем текущий размер обьетов, сколько в ней элементов
        if (args.length >= 2) { // если у нас есть второй аргумент
            //Integer.parseInt - преобразовываем количество обьектов в число
            count = Integer.parseInt(args[1]); // делаем количество объектов равным ему
        }

        // count раз выполняем действия:
        for (int i = 0; i < count; i++) {
            a.create(); // заполнить объект случайными данными
            a.update(); // ввести данные с клавиатуры
            a.read(); // напечатать данные об объекте на экран (на консоль)
        }
    }

    public static void main(String[] args) {
        ArrayList<Drink> a = new ArrayList<>();
        /*for (int i=0;i<3;i++) {
            Tea b = new Tea();
            b.create();
            a.add(b);
        }
        for (int i=0;i<3;i++) {
            Coffee b = new Coffee();
            b.create();
            a.add(b);
        }*/
        Tea b;
        b = new Tea();
        b.setName("Lipton");
        b.setCountry_manufacturer("Great Britain");
        b.setDistributor("Lipton Inc.");
        b.setPackage_type(1);
        b.setCost(100);
        a.add(b);

        b = new Tea();
        b.setName("Лисма");
        b.setCountry_manufacturer("Thailand");
        b.setDistributor("Lisma Inc.");
        b.setPackage_type(0);
        b.setCost(45);
        a.add(b);

        b = new Tea();
        b.setName("Brooke Bond");
        b.setCountry_manufacturer("Sri-Lanka");
        b.setDistributor("Brooke Bond Inc.");
        b.setPackage_type(0);
        b.setCost(70);
        a.add(b);

        Coffee f;
        f = new Coffee();
        f.setName("Nescafe");
        f.setCountry_manufacturer("Russia");
        f.setDistributor("Nestle");
        f.setCoffee_type(CoffeeType.arabica);
        f.setCost(90);
        a.add(f);

        f = new Coffee();
        f.setName("Jardin");
        f.setCountry_manufacturer("USA");
        f.setDistributor("Jardin Inc.");
        f.setCoffee_type(CoffeeType.robusta);
        f.setCost(200);
        a.add(f);

        for (int i = 0; i < a.size(); i++) {
            Drink n = a.get(i);
            n.read();
            System.out.println();
        }

        //ShoppingCart<Drink> cart = new ShoppingCart<>();
        ShoppingCart<Coffee> cart_cofe = new ShoppingCart<>();
        ShoppingCart<Tea> cart_chai = new ShoppingCart<>();

        Scanner sc = new Scanner(System.in);

        for (int k = 0; k < 2; k++) {
            //Найти напиток с указанным UUID товара
            System.out.println("Введите UUID товара который хотите купить: ");
            String UUID = sc.nextLine();
            int idx = -1;//если равно -1 то ничего не нашли
            for (int i = 0; i < a.size(); i++) {
                if (a.get(i).getID_Drink().toString().equals(UUID)) {
                    idx = i;
                    break;
                }
            }
            if (idx == -1) {
                System.out.println("Извините мы ничего не нашли!");
                return;
            }

            Drink n = a.get(idx);
            if (n instanceof Tea) { // если объект n является объектом класса Tea
                cart_chai.add((Tea) n);
            } else if (n instanceof Coffee) {
                cart_cofe.add((Coffee) n);
            }

            //положить в корзину найденный напиток
            //cart.add(a.get(idx));

        }
        //спросить ФИО и емайл, создать обьект Credentials
        System.out.println("Введите имя: ");
        String FirstName = sc.nextLine();
        System.out.println("Введите фамилию: ");
        String LastName = sc.nextLine();
        System.out.println("Введите отчество: ");
        String SecondName = sc.nextLine();
        System.out.println("Введите E_mail: ");
        String E_mail = sc.nextLine();

        Credentials cr = new Credentials(LastName, FirstName, SecondName
                , E_mail);

        //используя корзину и Credentials, создать заказ(обьект класса Order)
        //Order c = new Order(status_order.in_progress, Date.from(Instant.now()),
        //    null, cart, b);
        //c.print();
        Orders<Order> o = new Orders<>();
        o.Make_purchase(cart_cofe, cr);
        o.Make_purchase(cart_chai, cr);
        o.check_orders();
        o.print_orders();
    }
}
