package ru.myandin.generics;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {
    protected StatusOrder status;//поле статус заказа с именем status
    protected Date created_date;
    protected Date reserved_to;//поле Дата с именами время создания и ожидания
    protected ShoppingCart cart;//поле Корзина с именем корзина
    protected Credentials customer;//поле Пользователи с именем покупатель
    protected int id;
    protected static int next_id = 1;//номер следующего заказа 1

    public Order(StatusOrder status, Date created_date, Date reserved_to,
                 ShoppingCart cart, Credentials customer) {
        //this - ссылка
        //ссылка на экземпляр класса
        synchronized ((Object) next_id) {
            this.id = next_id++;//следующий после 1 заказа
        }
        this.setStatus(status);//ссылка на переменную экземпляра с именем status и присваиваем ей значение status
        this.created_date = created_date;
        this.reserved_to = reserved_to;
        this.cart = cart;
        this.customer = customer;

    }

    //метод print выводит на консоль
    public void print() {
        System.out.println("Номер заказа: " + getId());
        System.out.println(getStatus());
        System.out.println(getCreated_date());
        System.out.println(getReserved_to());
        getCart().print();
        getCustomer().print();
    }

    public StatusOrder getStatus() {
        return status;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public Date getReserved_to() {
        return reserved_to;
    }

    public ShoppingCart getCart() {
        return cart;
    }

    public Credentials getCustomer() {
        return customer;
    }

    public int getId() {
        return id;
    }

    public void setStatus(StatusOrder status) {
        this.status = status;
    }
}

