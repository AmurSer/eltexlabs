package ru.myandin.basics;

import java.util.UUID;

//implements - реализация(будут созданы все методы из ICrudAction)
@SuppressWarnings({"WeakerAccess", "unused"})
public abstract class Drink implements ICrudAction {
    protected UUID ID_Drink;

    protected String name;
    protected int cost; // цена
    protected static int counter_drinks = 0;
    protected String distributor; // фирма поставщика
    protected String country_manufacturer;//страна производитель

    // функция-конструктор
    public Drink() {
        ID_Drink = UUID.randomUUID();
        counter_drinks++;
    }


    // абстрактные методы для класса Drink
    public abstract void create();

    public abstract void read();

    public abstract void update();

    public abstract void delete();

    // автоматически сгенерированные методы
    // как их сделать: пкм - Refactor - Encapsulate Fields...
    public UUID getID_Drink() {
        return ID_Drink;
    }

    public String getName() {
        return name;
    }

    public int getCost() {
        return cost;
    }

    public String getDistributor() {
        return distributor;
    }

    public String getCountry_manufacturer() {
        return country_manufacturer;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public void setCountry_manufacturer(String country_manufacturer) {
        this.country_manufacturer = country_manufacturer;
    }
}
