package ru.myandin.basics;

//enum - перечисление
//enum с именем CoffeeType, далее добавлены - arabica, robusta
public enum CoffeeType {
    arabica, robusta
}
