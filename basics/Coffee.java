package ru.myandin.basics;

import java.io.Serializable;
import java.util.Scanner;

//extends - расширение(наследование от родительского класса Drink)
public class Coffee extends Drink implements Serializable {
    @SuppressWarnings("WeakerAccess")
    protected CoffeeType coffee_type;

    //super - вызов конструктора родительского класса
    public Coffee() {
        super();
    }

    //метод create – создание объекта со случайными значениями
    @Override
    public void create() {
        name = "Tovar " + (int) (Math.random() * 255);
        cost = (int) (Math.random() * 255);
        distributor = "Pyatorochka" + (int) (Math.random() * 255);
        country_manufacturer = "Russia" + (int) (Math.random() * 255);
        coffee_type = CoffeeType.values()[(int) (Math.random() * 1.99)];
    }

    //метод read – вывод данных на экран
    @Override
    public void read() {
        System.out.println(ID_Drink.toString());
        System.out.println(name + " (" + distributor + " из " + country_manufacturer + ")");
        System.out.println("Цена = " + cost + " руб., вид зёрен = " + coffee_type);
    }

    //метод update – ввод данных с клавиатуры
    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Введите название кофе: ");
        name = sc.nextLine();
        System.out.print("Введите цену: ");
        cost = sc.nextInt();
        sc.nextLine();
        System.out.print("Введите фирму поставщика: ");
        distributor = sc.nextLine();
        System.out.print("Введите страну производитель: ");
        country_manufacturer = sc.nextLine();
        System.out.print("Выберите 0 арабика или 1 робуста: ");
        coffee_type = CoffeeType.values()[sc.nextInt()];
        sc.nextLine();
    }

    //метод delete – принудительное зануление данных в объект
    @Override
    public void delete() {
        counter_drinks--;
        name = "";
        cost = 0;
        distributor = "";
        country_manufacturer = "";
        coffee_type = CoffeeType.arabica;
    }

    @SuppressWarnings("unused")
    public CoffeeType getCoffee_type() {
        return coffee_type;
    }

    public void setCoffee_type(CoffeeType coffee_type) {
        this.coffee_type = coffee_type;
    }
}
