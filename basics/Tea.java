package ru.myandin.basics;

import java.io.Serializable;
import java.util.Scanner;


public class Tea extends Drink implements Serializable {
    @SuppressWarnings("WeakerAccess")
    protected int package_type;//тип упаковки

    //вызов конструктора родительского класса
    public Tea() {
        super();
    }


    //метод create – создание объекта со случайными значениями
    @Override
    public void create() {
        name = "Товар " + (int) (Math.random() * 255);
        cost = (int) (Math.random() * 255);
        distributor = "Пятёрочка " + (int) (Math.random() * 255);
        country_manufacturer = "Россия " + (int) (Math.random() * 255);
        package_type = (int) (Math.random() * 255);
    }

    //метод read – вывод данных на экран
    @Override
    public void read() {
        System.out.println(ID_Drink.toString());
        System.out.println(name + " (" + distributor + " из " + country_manufacturer + ")");
        System.out.println("Цена = " + cost + " руб., вид упаковки = " + package_type);
    }

    //метод update – ввод данных с клавиатуры
    @Override
    public void update() {
        Scanner sc = new Scanner(System.in);
        name = sc.nextLine();
        cost = sc.nextInt();
        sc.nextLine();
        distributor = sc.nextLine();
        country_manufacturer = sc.nextLine();
        package_type = sc.nextInt();
        sc.nextLine();
    }

    //метод delete – принудительное зануление данных в объект
    @Override
    public void delete() {
        counter_drinks--;
        name = "";
        cost = 0;
        distributor = "";
        country_manufacturer = "";
        package_type = 0;
    }

    @SuppressWarnings("unused")
    public int getPackage_type() {
        return package_type;
    }

    public void setPackage_type(int package_type) {
        this.package_type = package_type;
    }
}
