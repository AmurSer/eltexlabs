package ru.myandin.basics;

public class Main {
    // args - массив аргументов командной строки
    // String[] args - массив со строками под названием args
    public static void main(String[] args) {
        // обьявили имя a для Napitok
        Drink a;
        //equals сравниваем обьект Cofe с аргументом
        switch (args[0]) {
            case "Coffee":
                //создаём новый пустой обьект Cofe для Napitok c именем a
                a = new Coffee();
                break;
            case "Tea":
                a = new Tea();
                break;
            default:
                System.out.println("нужно выбрать только чай или кофе");
                return;
        }

        int count = 1; // количество вводимых объектов. по умолчанию 1.
        //length - возвращаем текущий размер обьетов, сколько в ней элементов
        if (args.length >= 2) { // если у нас есть второй аргумент
            //Integer.parseInt - преобразовываем количество обьектов в число
            count = Integer.parseInt(args[1]); // делаем количество объектов равным ему
        }

        // count раз выполняем действия:
        for (int i = 0; i < count; i++) {
            a.create(); // заполнить объект случайными данными
            a.update(); // ввести данные с клавиатуры
            a.read(); // напечатать данные об объекте на экран (на консоль)
        }
    }
}
