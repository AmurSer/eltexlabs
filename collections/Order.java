package ru.myandin.collections;

import java.io.Serializable;
import java.util.Date;

public class Order implements Serializable {
    protected StatusOrder status;//поле статус заказа с именем status
    protected Date created_date, reserved_to;//поле Дата с именами время создания и ожидания
    protected ShoppingCart cart;//поле Корзина с именем корзина
    protected Credentials customer;//поле Пользователи с именем покупатель
    protected int id;
    protected static int next_id = 1;//номер следующего заказа 1

    public Order(StatusOrder status, Date created_date, Date reserved_to,
                 ShoppingCart cart, Credentials customer) {
        //this - ссылка
        //ссылка на экземпляр класса
        synchronized ((Object) next_id) {
            this.id = next_id++;//следующий после 1 заказа
        }
        this.status = status;//ссылка на переменную экземпляра с именем status и присваиваем ей значение status
        this.created_date = created_date;
        this.reserved_to = reserved_to;
        this.cart = cart;
        this.customer = customer;

    }

    //метод print выводит на консоль
    public void print() {
        System.out.println("Номер заказа: " + id);
        System.out.println(status);
        System.out.println(created_date);
        System.out.println(reserved_to);
        cart.print();
        customer.print();
    }
}

