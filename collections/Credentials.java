package ru.myandin.collections;

import java.io.Serializable;

public class Credentials implements Serializable {
    //переменные экземпляра класса Credentials(пользователи)
    protected int ID;
    protected String LastName;
    protected String FirstName;
    protected String SecondName;
    protected String E_mail;

    protected static int id_next = 1;

    //параметры
    public Credentials(String LastName, String FirstName,
                       String SecondName, String E_mail) {
        //this ссылка на текущий экземпляр класса Credentials
        synchronized ((Object) id_next) {
            this.ID = id_next;//ссылка на переменную ID и присваиваем ей значение параметра ID
            this.LastName = LastName;
            this.FirstName = FirstName;
            this.SecondName = SecondName;
            this.E_mail = E_mail;
            id_next++;
        }
    }

    //метод print выводит на консоль
    public void print() {
        System.out.println(ID);
        System.out.println(LastName);
        System.out.println(FirstName);
        System.out.println(SecondName);
        System.out.println(E_mail);
    }
}
