package ru.myandin.collections;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.LinkedList;

public class Orders {
    protected LinkedList<Order> order_list;

    public Orders() {
        order_list = new LinkedList<>();
    }

    public void Make_purchase(ShoppingCart c, Credentials d) {
        order_list.add(new Order(StatusOrder.in_progress, Date.from(Instant.now()),
                Date.from(Instant.now().plus(2, ChronoUnit.DAYS)), c, d));
    }

    public void check_orders() {
        for (int i = 0; i < order_list.size(); i++) {
            if (order_list.get(i).status.equals(StatusOrder.done) ||
                    Date.from(Instant.now()).after(order_list.get(i).reserved_to)) {
                order_list.remove(i);
            }
        }
    }

    public void print_orders() {
        for (int i = 0; i < order_list.size(); i++) {
            order_list.get(i).print();
        }
    }
}
