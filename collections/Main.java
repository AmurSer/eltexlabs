package ru.myandin.collections;

import ru.myandin.basics.Coffee;
import ru.myandin.basics.Drink;
import ru.myandin.basics.Tea;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ArrayList<Drink> a = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Drink b = new Tea();
            b.create();
            a.add(b);
        }
        for (int i = 0; i < 3; i++) {
            Drink b = new Coffee();
            b.create();
            a.add(b);
        }

        // напечатать все напитки (чтобы знать, из каких uuid выбирать)
        //System.out.println(a);
        for (int i = 0; i < a.size(); i++) {
            //a.get(i).read();
            Drink b = a.get(i);
            b.read();
        }

        // создать корзину
        ShoppingCart cart = new ShoppingCart();

        // для ввода
        Scanner sc = new Scanner(System.in);

        for (int k = 0; k < 2; k++) {
            // найти напиток с указанным uuid в а
            System.out.println("Введите UUID товара, который хотите купить: ");
            String UUID = sc.nextLine();
            int idx = -1; // если равно -1 то ничего не нашли
            //size - возвращаем текущий размер коллекций, сколько в ней элементов
            for (int i = 0; i < a.size(); i++) {
                if (a.get(i).getID_Drink().toString().equals(UUID)) {
                    idx = i;
                    break;
                }
            }
            if (idx == -1) {
                System.out.println("Извините мы ничего не нашли");
                return;
            }

            // положить в корзину найденный напиток
            cart.add(a.get(idx));
        }

        // спросить ФИО и емаил, создать объект Credentials
        System.out.print("Введите имя: ");
        String FirstName = sc.nextLine();
        System.out.print("Введите фамилию: ");
        String LastName = sc.nextLine();
        System.out.println("Введите отчество: ");
        String SecondName = sc.nextLine();
        System.out.println("Введите емайл: ");
        String E_mail = sc.nextLine();

        Credentials b = new Credentials(LastName, FirstName, SecondName, E_mail);

        // используя корзину и Credentials, создать заказ (объект класса Order).
        Order c = new Order(StatusOrder.in_progress, Date.from(Instant.now()), null, cart, b);
        c.print();

    }
}
