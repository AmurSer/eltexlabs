package ru.myandin.collections;

import ru.myandin.basics.Drink;

import java.time.Instant;
import java.util.*;

public class ShoppingCart {
    protected ArrayList<Drink> items_in_cart;
    // при добавлении в корзину - добавление в HashSet
    // при поиске по UUID - поиск по HashSet, а не по items_in_cart
    protected HashSet<Drink> items_to_find;

    protected HashMap<Date, Drink> items_by_date;

    ShoppingCart() {
        items_in_cart = new ArrayList<>();
        items_to_find = new HashSet<>();
        items_by_date = new HashMap<>();
    }

    public void add(Drink a) {
        items_in_cart.add(a);
        items_to_find.add(a);
        items_by_date.put(Date.from(Instant.now()), a);
    }

    public void print() {
        for (int i = 0; i < items_in_cart.size(); i++) {
            items_in_cart.get(i).read();
        }
    }

    public void delete(Drink a) {
        for (int i = 0; i < items_in_cart.size(); i++) {
            if (items_in_cart.get(i).equals(a)) {
                items_in_cart.remove(i);
            }
        }
    }

    public void delete(UUID b) {
        for (int i = 0; i < items_in_cart.size(); i++) {
            if (items_in_cart.get(i).getID_Drink().equals(b)) {
                //items_in_cart.get(i).delete();
                items_in_cart.remove(i);
            }
        }
    }

    public Drink find(UUID b) {
        Iterator<Drink> iterator = items_to_find.iterator();//
        while (iterator.hasNext()) {
            Drink n = iterator.next();
            if (n.getID_Drink().equals(b))
                return n;
        }
        // если ничего не нашли то возвращаемся
        return null;
    }
}


