package ru.myandin.streams;

import ru.myandin.generics.Order;
import ru.myandin.threads.ACheck;

import java.io.*;

import java.util.ArrayList;


public class AOrderManagerBinary extends AManageOrder {

    public AOrderManagerBinary(ArrayList<Order> orders) {
        super(orders);
    }

    @Override
    public void readByID(int id) throws Exception {
        //десериализация
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream(new FileInputStream("order_" + id + ".bin"));
            Order order = (Order) inputStream.readObject();
            synchronized (ACheck.mutex) {
                int order_id_arraylist = -1;
                for (int i = 0; i < order_list.size(); i++) {
                    if (order_list.get(i).getId() == id) order_id_arraylist = i;
                }
                if (order_id_arraylist == -1)
                    order_list.add(order);
                else
                    order_list.set(order_id_arraylist, order);
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    @Override
    public void saveByID(int id) throws Exception {
        //сериализация
        ObjectOutputStream outputStream = null;
        try {

            Order order = null;
            synchronized (ACheck.mutex) {
                for (int i = 0; i < order_list.size(); i++) {
                    if (order_list.get(i).getId() == id) order = order_list.get(i);
                }
            }
            if (order == null)
                throw new Exception("wrong id" + id);
            outputStream = new ObjectOutputStream(new FileOutputStream("order_" + id + ".bin"));
            outputStream.writeObject(order);
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }

    @Override
    public void readAll() throws Exception {
        //десериализация
        ObjectInputStream inputStream = null;
        try {
            inputStream = new ObjectInputStream(new FileInputStream("orders.bin"));
            ArrayList<Order> order_list_from_file = (ArrayList<Order>) inputStream.readObject();
            synchronized (ACheck.mutex) {
                order_list.clear();
                order_list.addAll(order_list_from_file);
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
    }

    @Override
    public void saveAll() throws Exception {
        //сериализация
        ObjectOutputStream outputStream = null;
        try {
            outputStream = new ObjectOutputStream(new FileOutputStream("orders.bin"));

            synchronized (ACheck.mutex) {
                outputStream.writeObject(order_list);
            }
        } finally {
            if (outputStream != null) {
                outputStream.close();
            }
        }
    }
}

