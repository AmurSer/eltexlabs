package ru.myandin.streams;

public interface IOrder {
    void readByID(int id) throws Exception;
    void saveByID(int id) throws Exception;
    void readAll() throws Exception;
    void saveAll() throws Exception;
}
