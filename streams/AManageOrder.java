package ru.myandin.streams;

import ru.myandin.generics.Order;

import java.util.ArrayList;

abstract public class AManageOrder implements IOrder {

    ArrayList<Order> order_list;

    //доступ к методу
    @SuppressWarnings("WeakerAccess")
    public AManageOrder(ArrayList<Order> orders) {
        order_list = orders;
    }

    public abstract void readByID(int id) throws Exception;

    public abstract void saveByID(int id) throws Exception;

    public abstract void readAll() throws Exception;

    public abstract void saveAll() throws Exception;
}
