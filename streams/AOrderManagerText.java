package ru.myandin.streams;

import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import ru.myandin.generics.Order;
import ru.myandin.threads.ACheck;


import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Scanner;

public class AOrderManagerText extends AManageOrder {

    private static Gson gson = new Gson();

    @SuppressWarnings("WeakerAccess")
    public AOrderManagerText(ArrayList<Order> orders) {
        super(orders);
    }

    @Override
    public void readByID(int id) throws Exception {
        //десериализация
        String content = new Scanner(new File("order_" + id + ".txt")).useDelimiter("\\Z").next();
        Order order = gson.fromJson(content, Order.class);
        synchronized (ACheck.mutex) {
            order_list.set(id, order);
        }
    }


    @Override
    public void saveByID(int id) throws Exception {
        //сериализация
        FileWriter writer = null;
        try {
            String out;
            synchronized (ACheck.mutex) {
                if (order_list.get(id) == null)
                    throw new IOException("wrong id" + id);
                writer = new FileWriter("order_" + id + ".txt");
                out = gson.toJson(order_list.get(id));
            }
            writer.write(out);

        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }

    @Override
    public void readAll() throws Exception {
        //десериализация
        String content = new Scanner(new File("orders.txt")).useDelimiter("\\Z").next();
        Type collectionType = new TypeToken<ArrayList<Order>>() {
        }.getType();
        synchronized (ACheck.mutex) {
            order_list = gson.fromJson(content, collectionType);
        }
    }

    @Override
    public void saveAll() throws Exception {
        //сериализация
        FileWriter writer = null;
        try {
            writer = new FileWriter("orders.txt");
            String out;
            synchronized (ACheck.mutex) {
                out = gson.toJson(order_list);
            }
            writer.write(out);
        } finally {
            if (writer != null) {
                writer.close();
            }
        }
    }
}

