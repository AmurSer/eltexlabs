package ru.myandin.streams;


import ru.myandin.basics.CoffeeType;
import ru.myandin.basics.Tea;
import ru.myandin.basics.Coffee;
import ru.myandin.basics.Drink;
import ru.myandin.generics.Order;
import ru.myandin.generics.StatusOrder;
import ru.myandin.threads.ACheck;
import ru.myandin.threads.Check_Done;
import ru.myandin.threads.Check_InProgress;
import ru.myandin.threads.Generator;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        ArrayList<Drink> a = new ArrayList<>();

        Tea b;
        b = new Tea();
        b.setName("Lipton");
        b.setCountry_manufacturer("Great Britain");
        b.setDistributor("Lipton Inc.");
        b.setPackage_type(1);
        b.setCost(100);
        a.add(b);

        b = new Tea();
        b.setName("Лисма");
        b.setCountry_manufacturer("Thailand");
        b.setDistributor("Lisma Inc.");
        b.setPackage_type(0);
        b.setCost(45);
        a.add(b);

        b = new Tea();
        b.setName("Brooke Bond");
        b.setCountry_manufacturer("Sri-Lanka");
        b.setDistributor("Brooke Bond Inc.");
        b.setPackage_type(0);
        b.setCost(70);
        a.add(b);

        Coffee f;
        f = new Coffee();
        f.setName("Nescafe");
        f.setCountry_manufacturer("Russia");
        f.setDistributor("Nestle");
        f.setCoffee_type(CoffeeType.arabica);
        f.setCost(90);
        a.add(f);

        f = new Coffee();
        f.setName("Jardin");
        f.setCountry_manufacturer("USA");
        f.setDistributor("Jardin Inc.");
        f.setCoffee_type(CoffeeType.robusta);
        f.setCost(200);
        a.add(f);

        for (int i = 0; i < a.size(); i++) {
            Drink n = a.get(i);
            n.read();
            System.out.println();
        }
        //создать корзину ArrayList c именем orders и присвоить новую пустую корзину
        ArrayList<Order> orders = new ArrayList<>();

        AOrderManagerText m_text = new AOrderManagerText(orders);
        AOrderManagerBinary m_0010 = new AOrderManagerBinary(orders);

        System.out.println("Доступные команды:");
        System.out.println("pause - приостановить, save - сохранить, read - загрузить.");

        //создать класс Check_InProgress обьявим имя c1 и присвоим новый пустой класс Check_InProgress
        Check_InProgress c1 = new Check_InProgress();
        c1.AssignCollection(orders);
        c1.start();

        Check_Done c2 = new Check_Done();
        c2.AssignCollection(orders);
        c2.start();

        Generator g1 = new Generator();
        g1.AssignCollection(orders);
        g1.SetAvailableDrinksCollection(a);
        g1.SetInterval(3500);//3.5 секунды
        g1.start();

        Generator g2 = new Generator();
        g2.AssignCollection(orders);
        g2.SetAvailableDrinksCollection(a);
        g2.SetInterval(4200);//4.2 секунды
        g2.start();

        Scanner sc = new Scanner(System.in);
        while (true) {
            String line = sc.nextLine();

            synchronized (ACheck.mutex) {
                try {
                    if (line.equalsIgnoreCase("pause")) {
                        ACheck.SetPaused(true);
                    } else if (line.equalsIgnoreCase("unpause")) {
                        ACheck.SetPaused(false);
                    } else if (line.equalsIgnoreCase("status")) {
                        System.out.print("W: ");//W - в ожидании
                        for (int i = 0; i < orders.size(); i++) {
                            if (orders.get(i).getStatus() == StatusOrder.in_progress)
                                System.out.print(orders.get(i).getId() + " ");
                        }
                        System.out.println();
                        System.out.print("D: ");//D - обработан
                        for (int i = 0; i < orders.size(); i++) {
                            if (orders.get(i).getStatus() == StatusOrder.done)
                                System.out.print(orders.get(i).getId() + " ");
                        }
                        System.out.println();
                        System.out.println();
                    }

                    if (line.substring(0, 4).equalsIgnoreCase("save")) {
                        int id;
                        try {
                            id = Integer.parseInt(line.substring(5));
                        } catch (Exception e) {
                            id = -1;
                        }
                        if (id == -1) {
                            m_0010.saveAll();
                            m_text.saveAll();
                            System.out.println("[i] Сохранение списка успешно завершено");
                        } else {
                            m_0010.saveByID(id);
                            m_text.saveByID(id);
                            System.out.println("[i] Сохранение заказа " + id + " успешно завершено");
                        }
                    } else if (line.substring(0, 4).equalsIgnoreCase("read")) {
                        int id;
                        try {
                            id = Integer.parseInt(line.substring(5));
                        } catch (Exception e) {
                            id = -1;
                        }
                        if (id == -1) {
                            m_0010.readAll();
                            m_text.readAll();
                            System.out.println("[i] Загрузка списка успешно завершена");
                        } else {
                            m_0010.readByID(id);
                            m_text.readByID(id);
                            System.out.println("[i] Загрузка заказа " + id + " успешно завершена");
                        }
                    }
                } catch (Exception e) {
                    System.out.println("[!] " + e.toString());
                    e.printStackTrace();
                }
            }
        }
    }
}
